using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace TrainingPlatformApi.Tests.Utils
{
    public class JwtUtils
    {
        /// <summary>
        /// 私鑰
        /// </summary>
        public const string SecurityKey = "AXBGKSqXDaAuWTNg7cRS59GY5BS9xf83nTBrZh83BPv5exMZv5Y3azWWhEZvzErWwvZKuYVyBHxVeSUE3vMcbSKDmweWwW6Cc5FFhcpbX2WnV7S73MgVGrTQQKF8gNczgBU6yBrqebC9TmWMPCehBgzcasuGxHmfdhc7rheKsXa7RaAxqdkt6f8gfSNH6C9Ppxm7tQm6EweFCS2BXYNpAXx8HzXNtRzWfDr7X9Q2r4qEh8H9wpGVGYfTq9tVnrU4";

        /// <summary>
        /// 網域
        /// </summary>
        public const string Domain = "http://localhost:5188";

        /// <summary>
        /// jwt 時效
        /// </summary>
        private int Minutes = 30;

        /// <summary>
        /// 生成 jwt 金鑰
        /// </summary>
        /// <param name="claim">加密項目</param>
        /// <param name="audience">接收者</param>
        /// <returns></returns>
        public string GenerateJwtToken(IEnumerable<Claim> claim)
        {
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey));
            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: Domain,
                    expires: DateTime.Now.AddMinutes(Minutes),
                    signingCredentials: creds,
                    claims: claim
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// jwt 解密
        /// </summary>
        /// <param name="token">令牌</param>
        /// <returns></returns>
        public Dictionary<string, string> DecryptJwtKey(string token)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            TokenValidationParameters param = new TokenValidationParameters();
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecurityKey));

            param.IssuerSigningKey = securityKey;
            param.ValidateIssuer = false;
            param.ValidateAudience = false;
            ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(token, param, out SecurityToken securityToken);

            foreach (var item in claimsPrincipal.Claims)
            {
                result.Add(item.Type, item.Value);
            }

            return result;
        }
    }
}
