using Dapper;
using MySql.Data.MySqlClient;

namespace TrainingPlatformApi.Models.InstrumentSettings
{
    public class InstrumentSettingListModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public InstrumentSettingListModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得儀器設定運動表現清單
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public IEnumerable<T> GetInstrumentSettingSportsPerformanceList<T>(int studentId)
        {
            sqlStr = @"SELECT instrument_settings.userAuthorizedId,
		                      instrument_settings.machinaryCode,
		                      instrument_settings.level,
		                      CASE instrument_settings.machinaryCode
			                       WHEN '1'  THEN '屈腿伸腿訓練機'
			                       WHEN '2'  THEN '推胸拉背訓練機'
			                       WHEN '3'  THEN '髖關節外展內收訓練機'
			                       WHEN '4'  THEN '蹬腿訓練機'
			                       WHEN '5'  THEN '上斜推拉訓練機'
			                       WHEN '6'  THEN '蝴蝶訓練機'
			                       WHEN '7'  THEN '坐式上臂訓練機'
			                       WHEN '8'  THEN '坐式轉體訓練機'
			                       WHEN '9'  THEN '深蹲訓練機'
			                       WHEN '10' THEN '坐式腹背訓練機'
		                       END AS machinaryCodeName,
		                      CONCAT(SUM(instrument_settings.power), ' 瓦特') AS power,
		                      CONCAT('0 - ', instrument_settings.maxRange) AS maxRange,
		                      CONCAT(SUBSTRING(instrument_settings.trainingDate, 1, 4), '-', SUBSTRING(instrument_settings.trainingDate, 5, 2), '-', SUBSTRING(instrument_settings.trainingDate, 7, 2)) AS trainingDate,
		                      ism.mark
	                     FROM instrument_settings
	                     LEFT JOIN (SELECT userAuthorizedId,
                                           trainingDate,
                                           mark
                                      FROM instrument_settings_mark
                                     WHERE type = 'SP') AS ism
	                       ON (    instrument_settings.userAuthorizedId = ism.userAuthorizedId
		                       AND instrument_settings.trainingDate = ism.trainingDate)
	                    WHERE     instrument_settings.userAuthorizedId = @userAuthorizedId
		                      AND instrument_settings.maxRange IS NOT NULL
		                      AND instrument_settings.consciousEffort IS NULL
	                    GROUP BY instrument_settings.userAuthorizedId,
			                     instrument_settings.machinaryCode,
			                     instrument_settings.maxRange,
			                     instrument_settings.level,
			                     instrument_settings.trainingDate,
			                     ism.mark
	                    ORDER BY instrument_settings.trainingDate DESC;
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }
    }
}
