using MySql.Data.MySqlClient;
using Dapper;
using System.ComponentModel.DataAnnotations;

using TrainingPlatformApi.Utils;
using TrainingPlatformApi.Settings;

namespace TrainingPlatformApi.Models.Student
{
    public class StudentModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public StudentModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得學員清單
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> GetStudentList<T>(string host, string imagePath)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               user_authorized.name,
                               user_authorized.gender,
                               CASE user_authorized.gender
                                    WHEN '男' THEN 1
                                    WHEN '女' THEN 0
                               END AS genderMequence,
                               YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - SubString(birthday, 1, 4) AS age,
                               CONCAT(bmr.height, '/', bmr.weight) AS posture,
                               user_authorized.phone,
                               user_authorized.address,
                               user_authorized.email,
                               CASE user_authorized.fileExtension
                                    WHEN 'png'  THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.png')
                                    WHEN 'gif'  THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.gif')
                                    WHEN 'jpeg' THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.jpeg')
                                    WHEN 'jpg'  THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.jpg')
                                END AS imageUrl
                          FROM user_authorized
                          LEFT JOIN (SELECT userAuthorizedId,
                                            height,
                                            weight
                                       FROM body_mass_record
                                      ORDER BY updateRecordDate DESC,
                                               updateRecordTime DESC
                                    ) AS bmr
                            ON (user_authorized.userAuthorizedId = bmr.userAuthorizedId)
                         WHERE     user_authorized.status   != '0'
                               AND user_authorized.identity = 'E'
                         ORDER BY user_authorized.userAuthorizedId  ASC
            ";

            var result = conn.Query<T>(sqlStr).ToList();

            return result;
        }

        /// <summary>
        /// 取得學員 aha/acsm 問卷
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> AhaAndAcsmQuestionnaire<T>()
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               aha_and_acsm_questionnaire.questionnaireId,
                               aha_and_acsm_questionnaire.part,
                               aha_and_acsm_questionnaire.subject
                          FROM user_authorized
                          LEFT JOIN (aha_and_acsm_questionnaire) ON (user_authorized.userAuthorizedId = aha_and_acsm_questionnaire.userAuthorizedId)
                         WHERE status != '0'
                         ORDER BY user_authorized.userAuthorizedId            ASC,
                                  aha_and_acsm_questionnaire.part             ASC,
                                  aha_and_acsm_questionnaire.subject          ASC,
                                  aha_and_acsm_questionnaire.updateRecordDate DESC,
                                  aha_and_acsm_questionnaire.updateRecordTime DESC
            ";

            var result = conn.Query<T>(sqlStr).ToList();

            return result;
        }
        
        /// <summary>
        /// 檢查學員是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId">學員 id</param>
        /// <returns></returns>
        public T InspactStudentExist<T>(int userAuthorizedId)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               gender,
                               phone,
                               YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - SubString(birthday, 1, 4) AS age
                          FROM user_authorized
                          WHERE     userAuthorizedId = @userAuthorizedId
                                AND status != 0
            ";

            var param = new
            {
                userAuthorizedId = userAuthorizedId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 停用學員
        /// </summary>
        /// <param name="userAuthorizedId">學員 id</param>
        /// <returns></returns>
        public int UpdateDisabledStudent(int userAuthorizedId)
        {
            sqlStr = $@"UPDATE user_authorized
                           SET status = '0'
                         WHERE userAuthorizedId = @userAuthorizedId
            ";

            var param = new
            {
                userAuthorizedId = userAuthorizedId
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }
        
        /// <summary>
        /// 檢查是否有重複電話號碼
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="phone">電話號碼</param>
        /// <returns></returns>
        public T InspactRepeatPhone<T>(string phone)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               phone
                          FROM user_authorized
                         WHERE phone = @phone
            ";

            var param = new
            {
                phone = phone
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 更新學員基本資訊
        /// </summary>
        /// <param name="recordId">紀錄修改人 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 學員修改基本資料 模型</param>
        /// <returns></returns>
        public int UpdateStudentInfo(int recordId, int studentId, string oldPhone, RequestStudentInfoModels obj)
        {
            if (oldPhone == obj.phone)
            {
                sqlStr = $@"UPDATE user_authorized
                               SET name             = @name,
                                   address          = @address,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordDate = @updateRecordDate,
                                   updateRecordTime = @updateRecordTime
                             WHERE userAuthorizedId = @userAuthorizedId
                ";
            }
            else
            {
                sqlStr = $@"UPDATE user_authorized
                               SET name             = @name,
                                   phone            = @phone,
                                   address          = @address,
                                   updateRecordId   = @updateRecordId,
                                   updateRecordDate = @updateRecordDate,
                                   updateRecordTime = @updateRecordTime
                             WHERE userAuthorizedId = @userAuthorizedId
                ";
            }

            string name = obj.name;
            string phone = obj.phone;
            string address = obj.address;
            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            object param = new
            {
                userAuthorizedId = studentId,
                name = name,
                phone = phone,
                address = address,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 檢查今日是否有體態紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今日日期</param>
        /// <returns></returns>
        public T InspactTodayBodyMassRecord<T>(int studentId, string today)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               body_mass_record.height,
                               body_mass_record.weight,
                               body_mass_record.newRecordDate,
                               body_mass_record.newRecordTime
                          FROM user_authorized
                          LEFT JOIN (body_mass_record)
                            ON (user_authorized.userAuthorizedId = body_mass_record.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId  = @userAuthorizedId
                               AND body_mass_record.userAuthorizedId = @userAuthorizedId
                               AND body_mass_record.newRecordDate    = @newRecordDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                newRecordDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增學員 身高體重
        /// </summary>
        /// <param name="recordId">紀錄修改人 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="obj">接收 學員修改基本資料 模型</param>
        /// <returns></returns>
        public int InsertBodyMassRecord(int recordId, int studentId, RequestStudentInfoModels obj)
        {
            string[] postureArr = obj.posture.Split("/");
            string height = postureArr[0];
            string weight = postureArr[1];
            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            sqlStr = $@"INSERT INTO body_mass_record
                        ( userAuthorizedId,  height,  weight,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime) VALUES
                        (@userAuthorizedId, @height, @weight, @newRecordId, @newRecordDate, @newRecordTime, @updateRecordId, @updateRecordDate, @updateRecordTime)
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                height = height,
                weight = weight,
                newRecordId = recordId,
                newRecordDate = nowDate,
                newRecordTime = nowTime,
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime
            };

            int insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新學員 身高體重
        /// </summary>
        /// <param name="recordId">紀錄修改人 id</param>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <param name="obj">接收 學員修改基本資料 模型</param>
        /// <returns></returns>
        public int UpdateBodyMassRecord(int recordId, int studentId, string today, RequestStudentInfoModels obj)
        {
            sqlStr = $@"UPDATE body_mass_record
                           SET height           = @height,
                               weight           = @weight,
                               updateRecordId   = @updateRecordId,
                               updateRecordTime = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND updateRecordDate = @updateRecordDate
            ";

            string[] postureArr = obj.posture.Split("/");
            string height = postureArr[0];
            string weight = postureArr[1];
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                height = height,
                weight = weight,
                updateRecordId = recordId,
                updateRecordDate = today,
                updateRecordTime = nowTime
            };

            int updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 取得一筆使用者基本資訊
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <returns></returns>
        public T GetOnlyStudentInfo<T>(int studentId, string host, string imagePath)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               user_authorized.phone,
                               user_authorized.name,
                               user_authorized.gender,
                               YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - SubString(user_authorized.birthday, 1, 4) AS age,
                               user_authorized.email,
                               user_authorized.address,
                               CASE user_authorized.fileExtension
                                    WHEN 'png'  THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.png')
                                    WHEN 'gif'  THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.gif')
                                    WHEN 'jpeg' THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.jpeg')
                                    WHEN 'jpg'  THEN CONCAT('{host}', '{imagePath}', '/', user_authorized.userAuthorizedId, '/', user_authorized.name, '.jpg')
                               END AS imageUrl,
                               CONCAT(body_mass_record.height, '/', body_mass_record.weight) AS posture
                          FROM user_authorized
                          LEFT JOIN (body_mass_record)
                            ON (user_authorized.userAuthorizedId = body_mass_record.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId   = @userAuthorizedId
                               AND body_mass_record.userAuthorizedId  = @userAuthorizedId
                               AND user_authorized.status            != '0'
                               AND user_authorized.identity =  'E'
                      ORDER BY body_mass_record.updateRecordDate DESC,
                               body_mass_record.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得學員最新一筆 aha 和 acsm 問卷
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public IEnumerable<T> GetOnlyAhaAndAcsmQuestionnaire<T>(int studentId)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               aha_and_acsm_questionnaire.questionnaireId,
                               aha_and_acsm_questionnaire.part,
                               aha_and_acsm_questionnaire.subject,
                               (SELECT MAX(updateRecordDate)
                                  FROM aha_and_acsm_questionnaire) AS updateRecordDate,
	                           aha_and_acsm_questionnaire.updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (aha_and_acsm_questionnaire)
                            ON (user_authorized.userAuthorizedId = aha_and_acsm_questionnaire.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId             = @userAuthorizedId
	                           AND aha_and_acsm_questionnaire.userAuthorizedId  = @userAuthorizedId
	                           AND user_authorized.status                      != '0'
                         ORDER BY aha_and_acsm_questionnaire.part    ASC,
                                  aha_and_acsm_questionnaire.subject ASC
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).ToList();

            return result;
        }
    }
}

/// <summary>
/// 接收 學員修改基本資料 模型
/// </summary>
public class RequestStudentInfoModels
{
    /// <summary>
    /// 學員名子
    /// </summary>
    [MaxLength(10)]
    public string name { get; set; } = "";

    /// <summary>
    /// 身高與體重(體態)
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+)\\/([0-9]+)$")]
    public string posture { get; set; } = "";

    /// <summary>
    /// 手機
    /// </summary>
    [Required]
    [RegularExpression("^[0-9]{8,10}$")]
    public string phone { get; set; } = "";

    /// <summary>
    /// 地址
    /// </summary>
    [MaxLength(50)]
    public string address { get; set; } = "";
}