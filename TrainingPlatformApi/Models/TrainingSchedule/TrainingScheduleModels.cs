using MySql.Data.MySqlClient;
using Dapper;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.Settings;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TrainingPlatformApi.Models.TrainingSchedule
{
    public class TrainingScheduleModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public TrainingScheduleModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查訓練排程與運動處方天數是否有誤差
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public T InspactTrainingScheduleRemainingFrequencyNotify<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr = $@"SET @cardiorespiratoryFitnessDayCount = (SELECT COUNT(*)
                                   FROM training_schedule
                                  WHERE     userAuthorizedId = @userAuthorizedId
                                        AND trainingType     = 1
                                        AND scheduleDate     >= @minWeekDate
                                        AND scheduleDate     <= @maxWeekDate
                                );

                         SET @cardiorespiratoryFitnessExerciseFrequency = (SELECT exerciseFrequency
			                                                                 FROM exercise_prescription_cardiorespiratory_fitness
                                                                            WHERE userAuthorizedId = @userAuthorizedId
                                                                            ORDER BY updateRecordDate DESC,
                                                                                     updateRecordTime DESC LIMIT 1
                                                                          );

                         SET @muscleStrengthAndMuscleEnduranceDayCount = (SELECT COUNT(*)
                                                                            FROM training_schedule
                                                                           WHERE     userAuthorizedId = @userAuthorizedId
                                                                                 AND trainingType     = 2
                                                                                 AND scheduleDate     >= @minWeekDate
                                                                                 AND scheduleDate     <= @maxWeekDate
                                                                         );

                         SET @muscleStrengthAndMuscleEnduranceExerciseFrequency = (SELECT exerciseFrequency
			                                                                         FROM exercise_prescription_muscle_strength_and_muscle_endurance
                                                                                    WHERE userAuthorizedId = @userAuthorizedId
                                                                                    ORDER BY updateRecordDate DESC,
                                                                                             updateRecordTime DESC LIMIT 1
                                                                                  );

                         SET @softnessDayCount = (SELECT COUNT(*)
                                                    FROM training_schedule
                                                   WHERE     userAuthorizedId = @userAuthorizedId
                                                         AND trainingType     = 3
                                                         AND scheduleDate     >= @minWeekDate
                                                         AND scheduleDate     <= @maxWeekDate
                                                 );

                         SET @softnessExerciseFrequency = (SELECT exerciseFrequency
			                                                 FROM exercise_prescription_softness
                                                            WHERE userAuthorizedId = @userAuthorizedId
                                                            ORDER BY updateRecordDate DESC,
                                                                      updateRecordTime DESC LIMIT 1
                                                          );

                         SET @balanceDayCount = (SELECT COUNT(*)
                                                    FROM training_schedule
                                                   WHERE     userAuthorizedId =  @userAuthorizedId
                                                         AND trainingType     =  4 
                                                         AND scheduleDate     >= @minWeekDate
                                                         AND scheduleDate     <= @maxWeekDate
                                                 );

                         SET @balanceExerciseFrequency = (SELECT exerciseFrequency
			                                                FROM exercise_prescription_balance
                                                           WHERE userAuthorizedId = @userAuthorizedId
                                                           ORDER BY updateRecordDate DESC,
                                                                    updateRecordTime DESC LIMIT 1
                                                          );

                         SELECT (@cardiorespiratoryFitnessExerciseFrequency - @cardiorespiratoryFitnessDayCount) AS cardiorespiratoryFitnessRemainingFrequency,
                                (@muscleStrengthAndMuscleEnduranceExerciseFrequency - @muscleStrengthAndMuscleEnduranceDayCount) AS muscleStrengthAndMuscleEnduranceRemainingFrequency,
                                (@softnessExerciseFrequency - @softnessDayCount) AS softnessRemainingFrequency,
                                (@balanceExerciseFrequency - @balanceDayCount) AS balanceRemainingFrequency
                           FROM dual;
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得運動處方-心肺適能剩餘排程天數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionCardiorespiratoryFitness<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SET @dayCount = (SELECT COUNT(*)
                                            FROM training_schedule
                                           WHERE     userAuthorizedId = @userAuthorizedId
                                                 AND trainingType     = 1
                                                 AND scheduleDate     >= @minWeekDate
                                                 AND scheduleDate     <= @maxWeekDate
                                         );

                         SET @exerciseFrequency = (SELECT exerciseFrequency
			                                          FROM exercise_prescription_cardiorespiratory_fitness
                                                     WHERE userAuthorizedId = @userAuthorizedId
                                                     ORDER BY updateRecordDate DESC,
                                                              updateRecordTime DESC LIMIT 1
                                                   );

                         SELECT @dayCount,
                                (@exerciseFrequency - @dayCount) AS remainingFrequency,
                                CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                exerciseFrequency,
                                CONCAT('每週 ', exerciseFrequency, ' 天') AS exerciseFrequencyText,
                                CONCAT(trainingUnitMinute, ' 分鐘/次') AS trainingUnitMinute,
                                sports,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime
                           FROM exercise_prescription_cardiorespiratory_fitness
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得訓練排程-心肺適能目標週已安排排程日期
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public IEnumerable<T> GetTargetWeekTrainingScheduleCardiorespiratoryFitness<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr = $@"SELECT CONCAT(SUBSTRING(scheduleDate, 1, 4), '-', SUBSTRING(scheduleDate, 5, 2), '-', SUBSTRING(scheduleDate, 7, 2)) AS scheduleDate
                          FROM training_schedule
                         WHERE     userAuthorizedId  = @userAuthorizedId
                               AND trainingType      = 1
						       AND scheduleDate     >= @minWeekDate
                               AND scheduleDate     <= @maxWeekDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 取得運動處方-肌力與肌耐力剩餘排程天數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionMuscleStrengthAndMuscleEndurance<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SET @dayCount = (SELECT COUNT(*)
                                            FROM training_schedule
                                           WHERE     userAuthorizedId = @userAuthorizedId
                                                 AND trainingType     = 2
                                                 AND scheduleDate     >= @minWeekDate
                                                 AND scheduleDate     <= @maxWeekDate
                                         );

                         SET @exerciseFrequency = (SELECT exerciseFrequency
			                                          FROM exercise_prescription_muscle_strength_and_muscle_endurance
                                                     WHERE userAuthorizedId = @userAuthorizedId
                                                     ORDER BY updateRecordDate DESC,
                                                              updateRecordTime DESC LIMIT 1
                                                   );

                         SELECT (@exerciseFrequency - @dayCount) AS remainingFrequency,
                                CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                exerciseFrequency,
                                CONCAT('每週 ', exerciseFrequency, ' 天') AS exerciseFrequencyText,
                                CONCAT(trainingUnitGroup, ' 組/天') AS trainingUnitGroup,
                                CONCAT(trainingUnitNumber, ' 次/組') AS trainingUnitNumber,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime
                           FROM exercise_prescription_muscle_strength_and_muscle_endurance
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得訓練排程-肌力與肌耐力目標週已安排排程日期
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public IEnumerable<T> GetTargetWeekTrainingScheduleMuscleStrengthAndMuscleEndurance<T>(int studentId, List<string> nextWeekDate)
        {

            sqlStr = $@"SELECT CONCAT(SUBSTRING(scheduleDate, 1, 4), '-', SUBSTRING(scheduleDate, 5, 2), '-', SUBSTRING(scheduleDate, 7, 2)) AS scheduleDate
                          FROM training_schedule
                         WHERE     userAuthorizedId  = @userAuthorizedId
                               AND trainingType      = 2
						       AND scheduleDate     >= @minWeekDate
                               AND scheduleDate     <= @maxWeekDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 取得運動處方-柔軟度剩餘排程天數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionSoftness<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SET @dayCount = (SELECT COUNT(*)
                                            FROM training_schedule
                                           WHERE     userAuthorizedId = @userAuthorizedId
                                                 AND trainingType     = 3
                                                 AND scheduleDate     >= @minWeekDate
                                                 AND scheduleDate     <= @maxWeekDate
                                         );

                         SET @exerciseFrequency = (SELECT exerciseFrequency
			                                          FROM exercise_prescription_softness
                                                     WHERE userAuthorizedId = @userAuthorizedId
                                                     ORDER BY updateRecordDate DESC,
                                                              updateRecordTime DESC LIMIT 1
                                                   );

                         SELECT (@exerciseFrequency - @dayCount) AS remainingFrequency,
                                CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                sports,
                                exerciseFrequency,
                                CONCAT('每週 ', exerciseFrequency, ' 天') AS exerciseFrequencyText,
                                CONCAT(trainingUnitMinute, ' 分鐘/次') AS trainingUnitMinute,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime
                           FROM exercise_prescription_softness
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得訓練排程-柔軟度目標週已安排排程日期
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public IEnumerable<T> GetTargetWeekTrainingScheduleSoftness<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr = $@"SELECT CONCAT(SUBSTRING(scheduleDate, 1, 4), '-', SUBSTRING(scheduleDate, 5, 2), '-', SUBSTRING(scheduleDate, 7, 2)) AS scheduleDate
                          FROM training_schedule
                         WHERE     userAuthorizedId  = @userAuthorizedId
                               AND trainingType      = 3
							   AND scheduleDate     >= @minWeekDate
                               AND scheduleDate     <= @maxWeekDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 取得運動處方-平衡剩餘排程天數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionBalance<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SET @dayCount = (SELECT COUNT(*)
                                            FROM training_schedule
                                           WHERE     userAuthorizedId = @userAuthorizedId
                                                 AND trainingType     = 4
                                                 AND scheduleDate     >= @minWeekDate
                                                 AND scheduleDate     <= @maxWeekDate
                                         );

                         SET @exerciseFrequency = (SELECT exerciseFrequency
			                                          FROM exercise_prescription_balance
                                                     WHERE userAuthorizedId = @userAuthorizedId
                                                     ORDER BY updateRecordDate DESC,
                                                              updateRecordTime DESC LIMIT 1
                                                   );

                         SELECT (@exerciseFrequency - @dayCount) AS remainingFrequency,
                                CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                sports,
                                exerciseFrequency,
                                CONCAT('每週 ', exerciseFrequency, ' 天') AS exerciseFrequencyText,
                                CONCAT(trainingUnitMinute, ' 分鐘/次') AS trainingUnitMinute,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime
                           FROM exercise_prescription_balance
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得運動處方-平衡剩餘目標週已安排排程日期
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="nextWeekDate"></param>
        /// <returns></returns>
        public IEnumerable<T> GetThisWeekTrainingScheduleBalance<T>(int studentId, List<string> nextWeekDate)
        {
            sqlStr = $@"SELECT CONCAT(SUBSTRING(scheduleDate, 1, 4), '-', SUBSTRING(scheduleDate, 5, 2), '-', SUBSTRING(scheduleDate, 7, 2)) AS scheduleDate
                          FROM training_schedule
                         WHERE     userAuthorizedId  = @userAuthorizedId
                               AND trainingType      = 4
							   AND scheduleDate     >= @minWeekDate
                               AND scheduleDate     <= @maxWeekDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                minWeekDate = nextWeekDate[0].Replace("-", ""),
                maxWeekDate = nextWeekDate[nextWeekDate.Count() - 1].Replace("-", "")
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 取得當日訓練排程-心肺適能實際完成次數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T GetDayTrainingScheduleCardiorespiratoryFitnessActualCompletion<T>(int studentId, RequestTrainingScheduleCardiorespiratoryFitnessActualCompletionModels obj)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                CONCAT(exerciseFrequency, ' 天/週') AS exerciseFrequency,
                                CONCAT(trainingUnitMinute, ' 分鐘/次') AS trainingUnitMinute,
                                sports,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime,
                                (SELECT actualCompletion
                                   FROM training_schedule
                                  WHERE     userAuthorizedId = @userAuthorizedId
                                        AND trainingType     = 1
                                        AND scheduleDate     = @scheduleDate
                                ) AS actualCompletion
                           FROM exercise_prescription_cardiorespiratory_fitness
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                scheduleDate = obj.scheduleDate
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得當日訓練排程-肌力與肌耐力實際完成次數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T GetDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion<T>(int studentId, RequestTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionModels obj)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT (@exerciseFrequency - @dayCount) AS remainingFrequency,
                                CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                CONCAT(exerciseFrequency, ' 天/週') AS exerciseFrequency,
                                CONCAT(trainingUnitGroup, ' 組/天') AS trainingUnitGroup,
                                CONCAT(trainingUnitNumber, ' 次/組') AS trainingUnitNumber,
                                trainingArea,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime,
                                (SELECT CONCAT('共 ', SUM(finish), ' 次') AS finish
                                   FROM instrument_settings
                                  WHERE     userAuthorizedId = @userAuthorizedId
                                        AND trainingDate     = @trainingDate
                                        AND maxRange         IS NULL
                                        AND maxSpeed         IS NULL
                                        AND consciousEffort  IS NOT NULL
                                  GROUP BY userAuthorizedId,
                                           trainingDate
                                ) AS finish
                           FROM exercise_prescription_muscle_strength_and_muscle_endurance
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                trainingDate = obj.scheduleDate.ToString("yyyyMMdd")
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得當日訓練排程-柔軟度實際完成次數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T GetDayTrainingScheduleSoftnessActualCompletion<T>(int studentId, RequestTrainingScheduleSoftnessActualCompletionModels obj)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                CONCAT(exerciseFrequency, ' 週/天') AS exerciseFrequency,
                                CONCAT(trainingUnitMinute, ' 分鐘/次') AS trainingUnitMinute,
                                sports,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime,
                                (SELECT actualCompletion
                                   FROM training_schedule
                                  WHERE     userAuthorizedId = @userAuthorizedId
                                        AND trainingType     = 3
                                        AND scheduleDate     = @scheduleDate
                                ) AS actualCompletion
                           FROM exercise_prescription_softness
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";


            var param = new
            {
                userAuthorizedId = studentId,
                scheduleDate = obj.scheduleDate
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得當日訓練排程-平衡實際完成次數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T GetDayTrainingScheduleBalanceActualCompletion<T>(int studentId, RequestTrainingScheduleBalanceActualCompletionModels obj)
        {
            sqlStr += ExercisePrescriptionUtils.ageSql;
            sqlStr += ExercisePrescriptionUtils.lowheartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.middleMaxHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMinHeartRateSql;
            sqlStr += ExercisePrescriptionUtils.highMaxHeartRateSql;
            sqlStr += $@"SELECT CASE exerciseIntensity
			                         WHEN '1' THEN '低強度運動'
                                     WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exerciseIntensity
		                             WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
                                     WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
	                             END AS heartRate,
                                CONCAT(exerciseFrequency, ' 週/天') AS exerciseFrequency,
                                CONCAT(trainingUnitMinute, ' 分鐘/次') AS trainingUnitMinute,
                                sports,
                                updateRecordId,
                                updateRecordDate,
                                updateRecordTime,
                                (SELECT actualCompletion
                                   FROM training_schedule
                                  WHERE     userAuthorizedId = @userAuthorizedId
                                        AND trainingType     = 4
                                        AND scheduleDate     = @scheduleDate
                                ) AS actualCompletion
                           FROM exercise_prescription_balance
                          WHERE userAuthorizedId = @userAuthorizedId
                          ORDER BY updateRecordDate DESC,
                                   updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                scheduleDate = obj.scheduleDate
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 檢查訓練排程心肺適能 或 肌力與肌耐力 或 柔軟度 或 平衡資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T InspactTrainingSchedule<T>(int studentId, int trainingType, DateTime scheduleDate)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               trainingType,
                               scheduleDate,
                               actualCompletion,
                               newRecordId,
                               newRecordDate,
                               newRecordTime,
                               updateRecordId,
                               updateRecordDate,
                               updateRecordTime
                          FROM training_schedule
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND trainingType     = @trainingType
                               AND scheduleDate     = @scheduleDate
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                trainingType = trainingType,
                scheduleDate = scheduleDate.ToString("yyyyMMdd")
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得過去 60 次訓練排程報表(心肺適能、柔軟度、平衡)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="trainingType"></param>
        /// <returns></returns>
        public IEnumerable<T> GetBeforeTrainingScheduleReport<T>(int studentId, int trainingType)
        {
            sqlStr = $@"SELECT userAuthorizedId,
                               CONCAT(SUBSTRING(scheduleDate, 1, 4), '-', SUBSTRING(scheduleDate, 5, 2), '-', SUBSTRING(scheduleDate, 7, 2)) AS scheduleDate,
                               trainingUnitMinute,
                               actualCompletion
                          FROM training_schedule
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND trainingType     = @trainingType
                         ORDER BY scheduleDate DESC LIMIT 60
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                trainingType = trainingType
            };

            var result = conn.Query<T>(sqlStr, param);

            return result;
        }

        /// <summary>
        /// 更新 心肺適能 排程實際完成次數
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="recordId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int UpdateTrainingScheduleActualCompletion(int studentId, int recordId, RequestUpdateTrainingScheduleModels obj)
        {
            sqlStr = $@"UPDATE training_schedule
                           SET actualCompletion     = @actualCompletion,
                               updateRecordId       = @updateRecordId,
                               updateRecordDate     = @updateRecordDate,
                               updateRecordTime     = @updateRecordTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND trainingType     = @trainingType
                               AND scheduleDate     = @scheduleDate
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                actualCompletion = obj.actualCompletion,
                trainingType = obj.trainingType,
                scheduleDate = obj.scheduleDate.ToString("yyyyMMdd"),
                updateRecordId = recordId,
                updateRecordDate = nowDate,
                updateRecordTime = nowTime,
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 檢查訓練排程與運動處方是否一致
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public T InspaceTrainingScheduleAndExercisePrescriptionDayContains<T>(int studentId, RequestInsertTrainingScheduleWeekModels obj)
        {
            sqlStr = $@"SELECT CASE @trainingType
                                    WHEN '1' THEN (SELECT exerciseFrequency
                                                     FROM exercise_prescription_cardiorespiratory_fitness
                                                    WHERE userAuthorizedId = @userAuthorizedId
                                                    ORDER BY updateRecordDate DESC,
                                                             updateRecordTime DESC LIMIT 1
                                    )
                                    WHEN '2' THEN (SELECT exerciseFrequency
                                                     FROM exercise_prescription_muscle_strength_and_muscle_endurance
                                                    WHERE userAuthorizedId = @userAuthorizedId
                                                    ORDER BY updateRecordDate DESC,
                                                             updateRecordTime DESC LIMIT 1
                                    )
                                    WHEN '3' THEN (SELECT exerciseFrequency
                                                     FROM exercise_prescription_softness
                                                    WHERE userAuthorizedId = @userAuthorizedId
                                                    ORDER BY updateRecordDate DESC,
                                                             updateRecordTime DESC LIMIT 1
                                    )
                                    WHEN '4' THEN (SELECT exerciseFrequency
                                                     FROM exercise_prescription_balance
                                                    WHERE userAuthorizedId = @userAuthorizedId
                                                    ORDER BY updateRecordDate DESC,
                                                             updateRecordTime DESC LIMIT 1
                                    )
                                END AS exerciseFrequency
                          FROM dual
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                trainingType = obj.trainingType
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 刪除舊的排程(週/日期)
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="nextWeek"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int DeleteTrainingScheduleWeek(int studentId, List<string> nextWeek, RequestInsertTrainingScheduleWeekModels obj)
        {
            sqlStr = $@"DELETE FROM training_schedule
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND trainingType     = @trainingType
                               AND scheduleDate     >= @minWeek
                               AND scheduleDate     <= @maxWeek
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                trainingType = obj.trainingType,
                minWeek = nextWeek[0].Replace("-", ""),
                maxWeek = nextWeek[nextWeek.Count - 1].Replace("-", "")
            };

            var deleteRow = conn.Execute(sqlStr, param);

            return deleteRow;
        }

        /// <summary>
        /// 新增排程日期
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="recordId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertTrainingScheduleWeek(int studentId, int recordId, RequestInsertTrainingScheduleWeekModels obj)
        {
            sqlStr = $@"INSERT INTO training_schedule ( userAuthorizedId,  trainingType,  scheduleDate,  trainingUnitMinute,  newRecordId,  newRecordDate,  newRecordTime,  updateRecordId,  updateRecordDate,  updateRecordTime)
                        SELECT @userAuthorizedId,
                               @trainingType,
                               @scheduleDate,
                               CASE @trainingType
			                        WHEN '1' THEN (SELECT trainingUnitMinute
						                             FROM exercise_prescription_cardiorespiratory_fitness
							                        WHERE userAuthorizedId = @userAuthorizedId
							                        ORDER BY updateRecordDate DESC,
									                         updateRecordTime DESC LIMIT 1
			                        )
			                        WHEN '3' THEN (SELECT trainingUnitMinute
							                         FROM exercise_prescription_softness
							                        WHERE userAuthorizedId = @userAuthorizedId
							                        ORDER BY updateRecordDate DESC,
									                         updateRecordTime DESC LIMIT 1
			                        )
			                        WHEN '4' THEN (SELECT trainingUnitMinute
							                         FROM exercise_prescription_balance
							                        WHERE userAuthorizedId = @userAuthorizedId
							                        ORDER BY updateRecordDate DESC,
									                         updateRecordTime DESC LIMIT 1
			                        )
		                        END AS trainingUnitMinute,
                               @newRecordId,
                               @newRecordDate,
                               @newRecordTime,
                               @updateRecordId,
                               @updateRecordDate,
                               @updateRecordTime
                          FROM dual
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            List<object> param = new List<object>();

            obj.scheduleDate.ForEach((item) =>
            {
                param.Add(new
                {
                    userAuthorizedId = studentId,
                    trainingType = obj.trainingType.ToString(),
                    scheduleDate = item.ToString("yyyyMMdd"),
                    newRecordId = recordId,
                    newRecordDate = nowDate,
                    newRecordTime = nowTime,
                    updateRecordId = recordId,
                    updateRecordDate = nowDate,
                    updateRecordTime = nowTime
                });
            });

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }
    }
    
    /// <summary>
    /// 接收 訓練排程-心肺適能實際完成模型
    /// </summary>
    public class RequestTrainingScheduleCardiorespiratoryFitnessActualCompletionModels
    {
        /// <summary>
        /// 訓練排程日期
        /// </summary>
        [Required]
        public DateTime scheduleDate { get; set; }
    }

    /// <summary>
    /// 接收 訓練排程-肌力與肌耐力實際完成模型
    /// </summary>
    public class RequestTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionModels
    {
        /// <summary>
        /// 訓練排程日期
        /// </summary>
        [Required]
        public DateTime scheduleDate { get; set; }
    }

    /// <summary>
    /// 接收 訓練排程-柔軟度實際完成模型
    /// </summary>
    public class RequestTrainingScheduleSoftnessActualCompletionModels
    {
        /// <summary>
        /// 訓練排程日期
        /// </summary>
        [Required]
        public DateTime scheduleDate { get; set; }
    }

    /// <summary>
    /// 接收 訓練排程-平衡實際完成模型
    /// </summary>
    public class RequestTrainingScheduleBalanceActualCompletionModels
    {
        /// <summary>
        /// 訓練排程日期
        /// </summary>
        [Required]
        public DateTime scheduleDate { get; set; }
    }

    /// <summary>
    /// 接收 更新訓練排程 排程日期
    /// </summary>
    public class RequestUpdateTrainingScheduleModels
    {
        /// <summary>
        /// 訓練類型
        /// </summary>
        [Required]
        [RegularExpression("^(1|2|3|4)*$")]
        public int trainingType { get; set; }

        /// <summary>
        /// 排程日期
        /// </summary>
        [Required]
        public DateTime scheduleDate { get; set; }

        /// <summary>
        /// 實際完成次數
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int actualCompletion { get; set; } = 0;
    }

    /// <summary>
    /// 接收 新增訓練排程 心肺適能 排程日期
    /// </summary>
    public class RequestInsertTrainingScheduleWeekModels
    {
        /// <summary>
        /// 訓練類型(心肺適能、肌力與肌耐力、柔軟度、平衡)
        /// </summary>
        [Required]
        [RegularExpression("^(1|2|3|4)*$")]
        public int trainingType { get; set; } = 0;

        /// <summary>
        /// 排程日期
        /// </summary>
        [MinLengthAttribute(0)]
        [MaxLengthAttribute(7)]
        public List<DateTime> scheduleDate { get; set; } = new List<DateTime>();
    }
}
