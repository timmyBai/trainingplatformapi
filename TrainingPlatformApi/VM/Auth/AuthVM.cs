using MySql.Data.MySqlClient;

namespace TrainingPlatformApi.VM.Auth
{
    /// <summary>
    /// 檢查是否有此使用者模型
    /// </summary>
    public class AuthLoginVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 帳號
        /// </summary>
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";

        /// <summary>
        /// 生日
        /// </summary>
        public string birthday { get; set; } = "";

        /// <summary>
        /// 年齡
        /// </summary>
        public int age { get; set; } = 0;

        /// <summary>
        /// 使用者照片
        /// </summary>
        public string imageUrl { get; set; } = "";

        /// <summary>
        /// 身分
        /// </summary>
        public string identity { get; set; } = "";

        /// <summary>
        /// 帳號狀態
        /// </summary>
        public int status { get; set; } = 0;

        /// <summary>
        /// 身高
        /// </summary>
        public double height { get; set; } = 0.0;

        /// <summary>
        /// 體重
        /// </summary>
        public double weight { get; set; } = 0.0;
        
        /// <summary>
        /// 角色
        /// </summary>
        public string role { get; set; } = "";
    }

    /// <summary>
    /// 產出令牌
    /// </summary>
    public class AuthLoginTokenVM
    {
        /// <summary>
        /// 訪問令牌
        /// </summary>
        public string accessToken { get; set; } = "";

        /// <summary>
        /// 令牌過期時間
        /// </summary>
        public long expires_in { get; set; } = 0;
    }

    /// <summary>
    /// 檢查授權帳號模型
    /// </summary>
    public class InspactAuthAccountVM
    {
        /// <summary>
        /// 帳號
        /// </summary>
        public string account { get; set; } = "";
    }

    /// <summary>
    /// 檢查授權手機模型
    /// </summary>
    public class InspactAuthPhoneVM
    {
        public string phone { get; set; } = "";
    }

    /// <summary>
    /// 最新註冊表 id
    /// </summary>
    public class NewAuthIdVM
    {
        public int newUserAuthorizedId { get; set; } = 0;
    }
}
