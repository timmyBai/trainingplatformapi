using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.ExercisePrescription;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.VM.ExercisePrescription;
using TrainingPlatformApi.VM.Student;

namespace TrainingPlatformApi.Controllers.ExercisePrescription
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class ExercisePrescriptionListController : ControllerBase
    {
        private readonly ILogger<ExercisePrescriptionListController> logger;
        private MySqlConnection? conn;

        public ExercisePrescriptionListController(ILogger<ExercisePrescriptionListController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得學員運動處方清單
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpGet]
        [Authorize]
        [Route("api/exercisePrescription/{studentId:int}/fetch")]
        public IActionResult ExercisePrescriptionList(int studentId)
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    ExercisePrescriptionListVM exercisePrescriptionListVM = new ExercisePrescriptionListVM();

                    // 取得學員運動處方-心肺適能歷史資料
                    var exercisePrescriptionCardiorespiratoryFitnessList = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionCardiorespiratoryFitnessHistory<ExercisePrescriptionCardiorespiratoryFitnessHistoryVM>(studentId);

                    if (exercisePrescriptionCardiorespiratoryFitnessList.Count() > 0)
                    {
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.userAuthorizedId = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].userAuthorizedId;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.exerciseIntensity = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].exerciseIntensity;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.heartRate = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].heartRate;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.sports = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].sports);
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.trainingFrequency = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].trainingFrequency;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.trainingUnitMinute = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].trainingUnitMinute;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.mark = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].mark;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.updateRecordId = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].updateRecordId;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.updateRecordDate = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].updateRecordDate;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.updateRecordTime = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[0].updateRecordTime;

                        for (int i = 0; i < exercisePrescriptionCardiorespiratoryFitnessList.Count(); i++)
                        {
                            if (i != 0)
                            {
                                exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.children.Add(new ExercisePrescriptionCardiorespiratoryFitnessGroupChildren()
                                {
                                    userAuthorizedId = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].userAuthorizedId,
                                    exerciseIntensity = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].exerciseIntensity,
                                    heartRate = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].heartRate,
                                    sports = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].sports),
                                    trainingFrequency = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].trainingFrequency,
                                    trainingUnitMinute = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].trainingUnitMinute,
                                    mark = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].mark,
                                    updateRecordId = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].updateRecordId,
                                    updateRecordDate = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].updateRecordDate,
                                    updateRecordTime = exercisePrescriptionCardiorespiratoryFitnessList.ToList()[i].updateRecordTime
                                });
                            }
                        }
                    }
                    else
                    {
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.userAuthorizedId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.exerciseIntensity = "-";
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.heartRate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.sports = new List<string>();
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.trainingFrequency = "-";
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.trainingUnitMinute = 0;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.mark = "";
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.updateRecordId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.updateRecordDate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionCardiorespiratoryFitnessGroup.updateRecordTime = "-";
                    }

                    // 取得學員運動處方-肌力與肌耐力歷史資料
                    var exercisePrescriptionMuscleStrengthAndMuscleEnduranceList = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistory<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceHistoryVM>(studentId);

                    if (exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.Count() > 0)
                    {
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.userAuthorizedId = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].userAuthorizedId;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.exerciseIntensity = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].exerciseIntensity;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.heartRate = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].heartRate;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.trainingFrequency = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].trainingFrequency;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.trainingArea = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].trainingArea);
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.mark = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].mark;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.updateRecordId = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].updateRecordId;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.updateRecordDate = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].updateRecordDate;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.updateRecordTime = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[0].updateRecordTime;
                        
                        for (int i = 0; i < exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.Count(); i++)
                        {
                            if (i != 0)
                            {
                                exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.children.Add(new ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildrenVM()
                                {
                                    userAuthorizedId = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].userAuthorizedId,
                                    exerciseIntensity = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].exerciseIntensity,
                                    heartRate = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].heartRate,
                                    trainingFrequency = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].trainingFrequency,
                                    trainingArea = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].trainingArea),
                                    mark = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].mark,
                                    updateRecordId = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].updateRecordId,
                                    updateRecordDate = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].updateRecordDate,
                                    updateRecordTime = exercisePrescriptionMuscleStrengthAndMuscleEnduranceList.ToList()[i].updateRecordTime
                                });
                            }
                        }
                    }
                    else
                    {
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.userAuthorizedId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.exerciseIntensity = "-";
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.heartRate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.trainingFrequency = "-";
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.trainingArea = new List<string>();
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.mark = "";
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.updateRecordId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.updateRecordDate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup.updateRecordTime = "-";
                    }

                    // 取得學員運動處方-柔軟度歷史資料
                    var exercisePrescriptionSoftness = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionSoftnessHistory<ExercisePrescriptionSoftnessHistoryVM>(studentId);

                    if (exercisePrescriptionSoftness.Count() > 0)
                    {
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.userAuthorizedId = exercisePrescriptionSoftness.ToList()[0].userAuthorizedId;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.exerciseIntensity = exercisePrescriptionSoftness.ToList()[0].exerciseIntensity;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.heartRate = exercisePrescriptionSoftness.ToList()[0].heartRate;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.sports = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionSoftness.ToList()[0].sports);
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.trainingFrequency = exercisePrescriptionSoftness.ToList()[0].trainingFrequency;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.trainingUnitMinute = exercisePrescriptionSoftness.ToList()[0].trainingUnitMinute;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.mark = exercisePrescriptionSoftness.ToList()[0].mark;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.updateRecordId = exercisePrescriptionSoftness.ToList()[0].updateRecordId;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.updateRecordDate = exercisePrescriptionSoftness.ToList()[0].updateRecordDate;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.updateRecordTime = exercisePrescriptionSoftness.ToList()[0].updateRecordTime;

                        for (int i = 0; i < exercisePrescriptionSoftness.Count(); i++)
                        {
                            if (i != 0)
                            {
                                exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.children.Add(new ExercisePrescriptionSoftnessGroupChildrenVM()
                                {
                                    userAuthorizedId = exercisePrescriptionSoftness.ToList()[i].userAuthorizedId,
                                    exerciseIntensity = exercisePrescriptionSoftness.ToList()[i].exerciseIntensity,
                                    heartRate = exercisePrescriptionSoftness.ToList()[i].heartRate,
                                    sports = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionSoftness.ToList()[i].sports),
                                    trainingFrequency = exercisePrescriptionSoftness.ToList()[i].trainingFrequency,
                                    trainingUnitMinute = exercisePrescriptionSoftness.ToList()[i].trainingUnitMinute,
                                    mark = exercisePrescriptionSoftness.ToList()[i].mark,
                                    updateRecordId = exercisePrescriptionSoftness.ToList()[i].updateRecordId,
                                    updateRecordDate = exercisePrescriptionSoftness.ToList()[i].updateRecordDate,
                                    updateRecordTime = exercisePrescriptionSoftness.ToList()[i].updateRecordTime
                                });
                            }
                        }
                    }
                    else
                    {
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.userAuthorizedId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.exerciseIntensity = "-";
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.heartRate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.sports = new List<string>();
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.trainingFrequency = "-";
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.trainingUnitMinute = 0;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.mark = "";
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.updateRecordId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.updateRecordDate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionSoftnessGroup.updateRecordTime = "-";
                    }

                    // 取得學員運動處方-平衡歷史資料
                    var exercisePrescriptionBalance = new ExercisePrescriptionListModels(conn).GetExercisePrescriptionBalanceHistory<ExercisePrescriptionBalanceHistoryVM>(studentId);

                    if (exercisePrescriptionBalance.Count() > 0)
                    {
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.userAuthorizedId = exercisePrescriptionBalance.ToList()[0].userAuthorizedId;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.exerciseIntensity = exercisePrescriptionBalance.ToList()[0].exerciseIntensity;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.heartRate = exercisePrescriptionBalance.ToList()[0].heartRate;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.sports = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionBalance.ToList()[0].sports);
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.trainingFrequency = exercisePrescriptionBalance.ToList()[0].trainingFrequency;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.trainingUnitMinute = exercisePrescriptionBalance.ToList()[0].trainingUnitMinute;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.mark = exercisePrescriptionBalance.ToList()[0].mark;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.updateRecordId = exercisePrescriptionBalance.ToList()[0].updateRecordId;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.updateRecordDate = exercisePrescriptionBalance.ToList()[0].updateRecordDate;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.updateRecordTime = exercisePrescriptionBalance.ToList()[0].updateRecordTime;
                        
                        for (int i = 0; i < exercisePrescriptionBalance.Count(); i++)
                        {
                            exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.children.Add(new ExercisePrescriptionBalanceGroupChildrenVM()
                            {
                                userAuthorizedId = exercisePrescriptionBalance.ToList()[i].userAuthorizedId,
                                exerciseIntensity = exercisePrescriptionBalance.ToList()[i].exerciseIntensity,
                                heartRate = exercisePrescriptionBalance.ToList()[i].heartRate,
                                sports = JsonConvert.DeserializeObject<List<string>>(exercisePrescriptionBalance.ToList()[i].sports),
                                trainingFrequency = exercisePrescriptionBalance.ToList()[i].trainingFrequency,
                                trainingUnitMinute = exercisePrescriptionBalance.ToList()[i].trainingUnitMinute,
                                mark = exercisePrescriptionBalance.ToList()[i].mark,
                                updateRecordId = exercisePrescriptionBalance.ToList()[i].updateRecordId,
                                updateRecordDate = exercisePrescriptionBalance.ToList()[i].updateRecordDate,
                                updateRecordTime = exercisePrescriptionBalance.ToList()[i].updateRecordTime
                            });
                        }
                    }
                    else
                    {
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.userAuthorizedId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.exerciseIntensity = "-";
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.heartRate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.sports = new List<string>();
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.trainingFrequency = "-";
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.trainingUnitMinute = 0;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.mark = "";
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.updateRecordId = 0;
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.updateRecordDate = "-";
                        exercisePrescriptionListVM.exercisePrescriptionBalanceGroup.updateRecordTime = "-";
                    }

                    result.isSuccess = true;
                    result.data = exercisePrescriptionListVM;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得運動處方-訓練階段
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpGet]
        [Authorize]
        [Route("api/exercisePrescriptionTrainingStage/{studentId:int}/latest")]
        public IActionResult GetLatestExercisePrescriptionTrainingStage(int studentId)
        {
            ResultModels result = new ResultModels();
            
            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var latestExercisePrescriptionTrainingStage = new ExercisePrescriptionListModels(conn).GetLatestExercisePrescriptionTrainingStage<LatestExercisePrescriptionListVM>(studentId);

                    result.isSuccess = true;
                    result.data = latestExercisePrescriptionTrainingStage;

                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
