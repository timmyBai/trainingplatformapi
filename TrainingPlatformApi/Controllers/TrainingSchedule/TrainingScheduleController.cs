using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net;

using TrainingPlatformApi.Models;
using TrainingPlatformApi.Models.Student;
using TrainingPlatformApi.Models.TrainingSchedule;
using TrainingPlatformApi.Settings;
using TrainingPlatformApi.Utils;
using TrainingPlatformApi.VM.Student;
using TrainingPlatformApi.VM.TrainingSchedul;

/*
 * 訓練排程
 */
namespace TrainingPlatformApi.Controllers.TrainingSchedule
{
    [Route("TrainingPlatform")]
    [ApiController]
    public class TrainingScheduleController : ControllerBase
    {
        private ILogger<TrainingScheduleController> logger;
        private MySqlConnection? conn;

        public TrainingScheduleController(ILogger<TrainingScheduleController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 檢查訓練排程排程天數是否有誤差發放通知
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/trainingSchedule/{studentId:int}/notify")]
        public IActionResult TrainingScheduleNotify(int studentId)
        {
            ResultModels result = new ResultModels();

            // 取得這週剩餘天數範圍
            List<string> correctThisWeekDateRemainingDayRange = new DayUtils().GetThisWeekRemainingDayRange();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    var trainingScheduleRemainingFrequencyNotify = new TrainingScheduleModels(conn).InspactTrainingScheduleRemainingFrequencyNotify<InspactTrainingScheduleRemainingFrequencyNotifyVM>(studentId, correctThisWeekDateRemainingDayRange);

                    TrainingScheduleNotifyVM trainingScheduleNotify = new TrainingScheduleNotifyVM();

                    if (trainingScheduleRemainingFrequencyNotify.cardiorespiratoryFitnessRemainingFrequency < 0)
                    {
                        trainingScheduleNotify.isNotify = true;
                        result.message += "心肺適能、";
                    }

                    if (trainingScheduleRemainingFrequencyNotify.muscleStrengthAndMuscleEnduranceRemainingFrequency < 0)
                    {
                        trainingScheduleNotify.isNotify = true;
                        result.message += "肌力與肌耐力、";
                    }

                    if (trainingScheduleRemainingFrequencyNotify.softnessRemainingFrequency < 0)
                    {
                        trainingScheduleNotify.isNotify = true;
                        result.message += "柔軟度、";
                    }

                    if (trainingScheduleRemainingFrequencyNotify.balanceRemainingFrequency < 0)
                    {
                        trainingScheduleNotify.isNotify = true;
                        result.message += "平衡、";
                    }

                    if (trainingScheduleNotify.isNotify)
                    {
                        result.message = result.message.Substring(0, result.message.Length - 1);
                        result.message += "，時間安排錯誤，請回到運動處方調整";
                    }

                    result.isSuccess = true;
                    result.data = trainingScheduleNotify;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-心肺適能這週剩餘排程天數
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/thisWeek/trainingSchedule/{studentId:int}/cardiorespiratoryFitness/remainingFrequency")]
        public IActionResult GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency(int studentId)
        {
            ResultModels result = new ResultModels();

            // 取得這週剩餘天數範圍
            List<string> correctThisWeekDateRemainingDayRange = new DayUtils().GetThisWeekRemainingDayRange();

            // 取得上週日期範圍
            List<string> prevWeekDateRange = new DayUtils().GetPreviousWeekDateRange();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得運動處方-心肺適能剩餘排程天數
                    var latestExercisePrescriptionCardiorespiratoryFitness = new TrainingScheduleModels(conn).GetLatestExercisePrescriptionCardiorespiratoryFitness<LatestExercisePrescriptionCardiorespiratoryFitnessVM>(studentId, correctThisWeekDateRemainingDayRange);
                    
                    // 如果從來都沒有安排任何運動處方-心肺適能
                    if (latestExercisePrescriptionCardiorespiratoryFitness == null)
                    {
                        result.isSuccess = false;
                        result.message = "NotSetUpAnyExercisePrescriptionCardiorespiratoryFitness";
                        logger.LogError("NotSetUpAnyExercisePrescriptionCardiorespiratoryFitness");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得訓練排程-心肺適能這週已排程日期
                    var getThisWeekTrainingScheduleCardiorespiratoryFitnessInfo = new TrainingScheduleModels(conn).GetTargetWeekTrainingScheduleCardiorespiratoryFitness<ThisWeekDateTrainingScheduleCardiorespiratoryFitnessVM>(studentId, correctThisWeekDateRemainingDayRange);

                    // 取得訓練排程-心肺適能上週已排程日期
                    var getPrevWeekTrainingScheduleCardiorespiratoryFitnessInfo = new TrainingScheduleModels(conn).GetTargetWeekTrainingScheduleCardiorespiratoryFitness<PrevWeekDateTrainingScheduleCardiorespiratoryFitnessVM>(studentId, prevWeekDateRange);

                    ConvertThisWeekDateTrainingScheduleCardiorespiratoryFitnessVM convertThisWeekDateTrainingScheduleCardiorespiratoryFitness = new ConvertThisWeekDateTrainingScheduleCardiorespiratoryFitnessVM();

                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.remainingFrequency = latestExercisePrescriptionCardiorespiratoryFitness.remainingFrequency;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.exerciseIntensity = latestExercisePrescriptionCardiorespiratoryFitness.exerciseIntensity;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.heartRate = latestExercisePrescriptionCardiorespiratoryFitness.heartRate;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.exerciseFrequency = latestExercisePrescriptionCardiorespiratoryFitness.exerciseFrequency;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.exerciseFrequencyText = latestExercisePrescriptionCardiorespiratoryFitness.exerciseFrequencyText;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.trainingUnitMinute = latestExercisePrescriptionCardiorespiratoryFitness.trainingUnitMinute;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.sports = JsonConvert.DeserializeObject<List<string>>(latestExercisePrescriptionCardiorespiratoryFitness.sports);
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.updateRecordId = latestExercisePrescriptionCardiorespiratoryFitness.updateRecordId;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.updateRecordDate = latestExercisePrescriptionCardiorespiratoryFitness.updateRecordDate;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.updateRecordTime = latestExercisePrescriptionCardiorespiratoryFitness.updateRecordTime;
                    convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.thisWeekDateRemainingDayRange = correctThisWeekDateRemainingDayRange;

                    // 如果這週尚未安排訓練排程
                    if (getThisWeekTrainingScheduleCardiorespiratoryFitnessInfo == null)
                    {
                        for (int i = 0; i < correctThisWeekDateRemainingDayRange.Count(); i++)
                        {
                            var convertThisDate = Convert.ToDateTime(correctThisWeekDateRemainingDayRange[i]);
                            var convertThisDateWeek = Convert.ToInt32(convertThisDate.DayOfWeek);
                            var prevDate = convertThisDate.AddDays(convertThisDateWeek - convertThisDateWeek - 7);

                            for (int j = 0; j < getPrevWeekTrainingScheduleCardiorespiratoryFitnessInfo.Count(); j++)
                            {
                                string prevDateString = prevDate.ToString("yyyy-MM-dd");

                                // 如果上週星期有安排訓練排程
                                if (getPrevWeekTrainingScheduleCardiorespiratoryFitnessInfo.ToList()[i].scheduleDate == prevDateString)
                                {
                                    // 預設排程長度 小於 運動處方每週天數
                                    if (convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.defaultTrainingScheduleDate.Count() <= latestExercisePrescriptionCardiorespiratoryFitness.exerciseFrequency)
                                    {
                                        // 加入預設訓練排程
                                        convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.defaultTrainingScheduleDate.Add(correctThisWeekDateRemainingDayRange[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // 新增已列入排程日期
                        getThisWeekTrainingScheduleCardiorespiratoryFitnessInfo.ToList().ForEach((item) =>
                        {
                            convertThisWeekDateTrainingScheduleCardiorespiratoryFitness.scheduleDate.Add(item.scheduleDate);
                        });
                    }

                    result.isSuccess = true;
                    result.data = convertThisWeekDateTrainingScheduleCardiorespiratoryFitness;
                    logger.LogInformation("GetTrainingScheduleCardiorespiratoryFitnessRemainingFrequencySuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-肌力與肌耐力這週剩餘排程天數
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/thisWeek/trainingSchedule/{studentId:int}/muscleStrengthAndMuscleEndurance/remainingFrequency")]
        public IActionResult GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency(int studentId)
        {
            ResultModels result = new ResultModels();

            // 取得這週剩餘天數範圍
            List<string> correctThisWeekDateRemainingDayRange = new DayUtils().GetThisWeekRemainingDayRange();

            // 取得上週日期範圍
            List<string> prevWeekDateRange = new DayUtils().GetPreviousWeekDateRange();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得運動處方-肌力與肌耐力剩餘排程天數
                    var latestExercisePrescriptionMuscleStrengthAndMuscleEndurance = new TrainingScheduleModels(conn).GetLatestExercisePrescriptionMuscleStrengthAndMuscleEndurance<LatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceVM>(studentId, correctThisWeekDateRemainingDayRange);

                    // 如果從來都沒有安排任何運動處方-肌力與肌耐力
                    if (latestExercisePrescriptionMuscleStrengthAndMuscleEndurance == null)
                    {
                        result.isSuccess = false;
                        result.message = "NotSetUpAnyExercisePrescriptionMuscleStrengthAndMuscleEndurance";
                        logger.LogError("NotSetUpAnyExercisePrescriptionMuscleStrengthAndMuscleEndurance");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得訓練排程-肌力與肌耐力這週已排程日期
                    var getThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceInfo = new TrainingScheduleModels(conn).GetTargetWeekTrainingScheduleMuscleStrengthAndMuscleEndurance<ThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEnduranceVM>(studentId, correctThisWeekDateRemainingDayRange);

                    // 取得訓練排程-肌力與肌耐力上週已排程日期
                    var getPrevWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceInfo = new TrainingScheduleModels(conn).GetTargetWeekTrainingScheduleMuscleStrengthAndMuscleEndurance<PrevWeekDateTrainingScheduleMuscleStrengthAndMuscleEnduranceVM>(studentId, prevWeekDateRange);

                    ConvertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEnduranceVM convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance = new ConvertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEnduranceVM();

                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.remainingFrequency = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.remainingFrequency;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.exerciseIntensity = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.exerciseIntensity;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.heartRate = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.heartRate;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.exerciseFrequency = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.exerciseFrequency;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.exerciseFrequencyText = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.exerciseFrequencyText;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.trainingUnitGroup = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.trainingUnitGroup;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.trainingUnitNumber = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.trainingUnitNumber;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.updateRecordId = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.updateRecordId;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.updateRecordDate = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.updateRecordDate;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.updateRecordTime = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.updateRecordTime;
                    convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.thisWeekDateRemainingDayRange = correctThisWeekDateRemainingDayRange;

                    // 如果這週尚未安排訓練排程
                    if (getThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceInfo == null)
                    {
                        for (int i = 0; i < correctThisWeekDateRemainingDayRange.Count(); i++)
                        {
                            var convertThisDate = Convert.ToDateTime(correctThisWeekDateRemainingDayRange[i]);
                            var convertThisDateWeek = Convert.ToInt32(convertThisDate.DayOfWeek);
                            var prevDate = convertThisDate.AddDays(convertThisDateWeek - convertThisDateWeek - 7);

                            for (int j = 0; j < getPrevWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceInfo.Count(); j++)
                            {
                                string prevDateString = prevDate.ToString("yyyy-MM-dd");

                                // 如果上週星期有安排訓練排程
                                if (getPrevWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceInfo.ToList()[i].scheduleDate == prevDateString)
                                {
                                    // 預設排程長度 小於 運動處方每週天數
                                    if (convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.defaultTrainingScheduleDate.Count() <= latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.exerciseFrequency)
                                    {
                                        // 加入預設訓練排程
                                        convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.defaultTrainingScheduleDate.Add(correctThisWeekDateRemainingDayRange.ToList()[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // 新增已列入排程日期
                        getThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceInfo.ToList().ForEach((item) =>
                        {
                            convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance.scheduleDate.Add(item.scheduleDate);
                        });
                    }

                    result.isSuccess = true;
                    result.data = convertThisWeekDateTrainingScheduleMuscleStrengthAndMuscleEndurance;
                    logger.LogInformation("GetTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencySuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-柔軟度這週剩餘排程天數
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/thisWeek/trainingSchedule/{studentId:int}/softness/remainingFrequency")]
        public IActionResult GetThisWeekTrainingScheduleSoftnessRemainingFrequency(int studentId)
        {
            ResultModels result = new ResultModels();

            // 取得這週剩餘天數範圍
            List<string> correctThisWeekDateRemainingDayRange = new DayUtils().GetThisWeekRemainingDayRange();

            // 取得上週日期範圍
            List<string> prevWeekDateRange = new DayUtils().GetPreviousWeekDateRange();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得運動處方-柔軟度剩餘排程天數
                    var latestExercisePrescriptionSoftness = new TrainingScheduleModels(conn).GetLatestExercisePrescriptionSoftness<LatestExercisePrescriptionSoftnessVM>(studentId, correctThisWeekDateRemainingDayRange);

                    // 如果從來都沒有安排任何運動處方-柔軟度
                    if (latestExercisePrescriptionSoftness == null)
                    {
                        result.isSuccess = false;
                        result.message = "NotSetUpAnyExercisePrescriptionSoftness";
                        logger.LogError("NotSetUpAnyExercisePrescriptionSoftness");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得訓練排程-柔軟度這週已排程日期
                    var getThisWeekTrainingScheduleSoftnessInfo = new TrainingScheduleModels(conn).GetTargetWeekTrainingScheduleSoftness<ThisWeekDateTrainingScheduleSoftnessVM>(studentId, correctThisWeekDateRemainingDayRange);

                    // 取得訓練排程-柔軟度上週已排程日期
                    var getPrevWeekTrainingScheduleSoftnessInfo = new TrainingScheduleModels(conn).GetTargetWeekTrainingScheduleSoftness<PrevWeekDateTrainingScheduleSoftnessVM>(studentId, prevWeekDateRange);

                    ConvertThisWeekDateTrainingScheduleSoftnessVM convertThisWeekDateTrainingScheduleSoftness = new ConvertThisWeekDateTrainingScheduleSoftnessVM();

                    convertThisWeekDateTrainingScheduleSoftness.remainingFrequency = latestExercisePrescriptionSoftness.remainingFrequency;
                    convertThisWeekDateTrainingScheduleSoftness.exerciseIntensity = latestExercisePrescriptionSoftness.exerciseIntensity;
                    convertThisWeekDateTrainingScheduleSoftness.heartRate = latestExercisePrescriptionSoftness.heartRate;
                    convertThisWeekDateTrainingScheduleSoftness.exerciseFrequency = latestExercisePrescriptionSoftness.exerciseFrequency;
                    convertThisWeekDateTrainingScheduleSoftness.exerciseFrequencyText = latestExercisePrescriptionSoftness.exerciseFrequencyText;
                    convertThisWeekDateTrainingScheduleSoftness.trainingUnitMinute = latestExercisePrescriptionSoftness.trainingUnitMinute;
                    convertThisWeekDateTrainingScheduleSoftness.sports = JsonConvert.DeserializeObject<List<string>>(latestExercisePrescriptionSoftness.sports);
                    convertThisWeekDateTrainingScheduleSoftness.updateRecordId = latestExercisePrescriptionSoftness.updateRecordId;
                    convertThisWeekDateTrainingScheduleSoftness.updateRecordDate = latestExercisePrescriptionSoftness.updateRecordDate;
                    convertThisWeekDateTrainingScheduleSoftness.updateRecordTime = latestExercisePrescriptionSoftness.updateRecordTime;
                    convertThisWeekDateTrainingScheduleSoftness.thisWeekDateRemainingDayRange = correctThisWeekDateRemainingDayRange;

                    // 如果這週尚未安排訓練排程
                    if (getThisWeekTrainingScheduleSoftnessInfo == null)
                    {
                        for (int i = 0; i < correctThisWeekDateRemainingDayRange.Count(); i++)
                        {
                            var convertThisDate = Convert.ToDateTime(correctThisWeekDateRemainingDayRange[i]);
                            var convertThisDateWeek = Convert.ToInt32(convertThisDate.DayOfWeek);
                            var prevDate = convertThisDate.AddDays(convertThisDateWeek - convertThisDateWeek - 7);

                            for (int j = 0; j < getPrevWeekTrainingScheduleSoftnessInfo.Count(); j++)
                            {
                                string prevDateString = prevDate.ToString("yyyy-MM-dd");

                                // 如果上週星期有安排訓練排程
                                if (getPrevWeekTrainingScheduleSoftnessInfo.ToList()[i].scheduleDate == prevDateString)
                                {
                                    // 預設排程長度 小於 運動處方每週天數
                                    if (convertThisWeekDateTrainingScheduleSoftness.defaultTrainingScheduleDate.Count() <= latestExercisePrescriptionSoftness.exerciseFrequency)
                                    {
                                        // 加入預設訓練排程
                                        convertThisWeekDateTrainingScheduleSoftness.defaultTrainingScheduleDate.Add(correctThisWeekDateRemainingDayRange.ToList()[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // 新增已列入排程日期
                        getThisWeekTrainingScheduleSoftnessInfo.ToList().ForEach((item) =>
                        {
                            convertThisWeekDateTrainingScheduleSoftness.scheduleDate.Add(item.scheduleDate);
                        });
                    }

                    result.isSuccess = true;
                    result.data = convertThisWeekDateTrainingScheduleSoftness;
                    logger.LogInformation("GetTrainingScheduleSoftnessRemainingFrequencySuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-平衡這週剩餘排程天數
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/thisWeek/trainingSchedule/{studentId:int}/balance/remainingFrequency")]
        public IActionResult GetThisWeekTrainingScheduleBalanceRemainingFrequency(int studentId)
        {
            ResultModels result = new ResultModels();

            // 取得這週剩餘天數範圍
            List<string> correctThisWeekDateRemainingDayRange = new DayUtils().GetThisWeekRemainingDayRange();

            // 取得上週日期範圍
            List<string> prevWeekDateRange = new DayUtils().GetPreviousWeekDateRange();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得運動處方-平衡剩餘排程天數
                    var latestExercisePrescriptionBalance = new TrainingScheduleModels(conn).GetLatestExercisePrescriptionBalance<LatestExercisePrescriptionBalanceVM>(studentId, correctThisWeekDateRemainingDayRange);

                    // 如果從來都沒有安排任何運動處方-平衡
                    if (latestExercisePrescriptionBalance == null)
                    {
                        result.isSuccess = true;
                        result.message = "NotSetUpAnyExercisePrescriptionBalance";
                        logger.LogError("NotSetUpAnyExercisePrescriptionBalance");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得訓練排程-平衡這週已排程日期
                    var getThisWeekTrainingScheduleBalanceInfo = new TrainingScheduleModels(conn).GetThisWeekTrainingScheduleBalance<ThisWeekDateTrainingScheduleBalanceVM>(studentId, correctThisWeekDateRemainingDayRange);

                    // 取得訓練排程-平衡上週已排程日期
                    var getPrevWeekTrainingScheduleBalanceInfo = new TrainingScheduleModels(conn).GetThisWeekTrainingScheduleBalance<PrevWeekDateTrainingScheduleBalanceVM>(studentId, prevWeekDateRange);

                    ConvertThisWeekDateTrainingScheduleBalanceVM convertThisWeekDateTrainingScheduleBalance = new ConvertThisWeekDateTrainingScheduleBalanceVM();

                    convertThisWeekDateTrainingScheduleBalance.remainingFrequency = latestExercisePrescriptionBalance.remainingFrequency;
                    convertThisWeekDateTrainingScheduleBalance.exerciseIntensity = latestExercisePrescriptionBalance.exerciseIntensity;
                    convertThisWeekDateTrainingScheduleBalance.heartRate = latestExercisePrescriptionBalance.heartRate;
                    convertThisWeekDateTrainingScheduleBalance.exerciseFrequency = latestExercisePrescriptionBalance.exerciseFrequency;
                    convertThisWeekDateTrainingScheduleBalance.exerciseFrequencyText = latestExercisePrescriptionBalance.exerciseFrequencyText;
                    convertThisWeekDateTrainingScheduleBalance.trainingUnitMinute = latestExercisePrescriptionBalance.trainingUnitMinute;
                    convertThisWeekDateTrainingScheduleBalance.sports = JsonConvert.DeserializeObject<List<string>>(latestExercisePrescriptionBalance.sports);
                    convertThisWeekDateTrainingScheduleBalance.updateRecordId = latestExercisePrescriptionBalance.updateRecordId;
                    convertThisWeekDateTrainingScheduleBalance.updateRecordDate = latestExercisePrescriptionBalance.updateRecordDate;
                    convertThisWeekDateTrainingScheduleBalance.updateRecordTime = latestExercisePrescriptionBalance.updateRecordTime;
                    convertThisWeekDateTrainingScheduleBalance.thisWeekDateRemainingDayRange = correctThisWeekDateRemainingDayRange;

                    // 如果這週尚未安排訓練排程
                    if (getThisWeekTrainingScheduleBalanceInfo == null)
                    {
                        for (int i = 0; i < correctThisWeekDateRemainingDayRange.Count(); i++)
                        {
                            var convertThisDate = Convert.ToDateTime(correctThisWeekDateRemainingDayRange.ToList()[i]);
                            var convertThisDateWeek = Convert.ToInt32(convertThisDate.DayOfWeek);
                            var prevDate = convertThisDate.AddDays(convertThisDateWeek - convertThisDateWeek - 7);

                            for (int j = 0; j < getPrevWeekTrainingScheduleBalanceInfo.Count(); j++)
                            {
                                string prevDateString = prevDate.ToString("yyyy-MM-dd");

                                // 如果上週星期有安排訓練排程
                                if (getPrevWeekTrainingScheduleBalanceInfo.ToList()[i].scheduleDate == prevDateString)
                                {
                                    // 預設排程長度 小於 運動處方每週天數
                                    if (convertThisWeekDateTrainingScheduleBalance.defaultTrainingScheduleDate.Count() <= latestExercisePrescriptionBalance.exerciseFrequency)
                                    {
                                        // 加入預設訓練排程
                                        convertThisWeekDateTrainingScheduleBalance.defaultTrainingScheduleDate.Add(correctThisWeekDateRemainingDayRange.ToList()[i]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // 新增已列入排程日期
                        getThisWeekTrainingScheduleBalanceInfo.ToList().ForEach((item) =>
                        {
                            convertThisWeekDateTrainingScheduleBalance.scheduleDate.Add(item.scheduleDate);
                        });
                    }

                    result.isSuccess = true;
                    result.data = convertThisWeekDateTrainingScheduleBalance;
                    logger.LogInformation("GetTrainingScheduleBalanceRemainingFrequencySuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-心肺適能實際完成次數
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/day/trainingSchedule/{studentId:int}/cardiorespiratoryFitness/actualCompletion")]
        public IActionResult GetDayTrainingScheduleCardiorespiratoryFitnessActualCompletion(int studentId, RequestTrainingScheduleCardiorespiratoryFitnessActualCompletionModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();

                using(conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得當日訓練排程-心肺適能實際完成次數
                    var dayTrainingScheduleCardiorespiratoryFitnessActualCompletion = new TrainingScheduleModels(conn).GetDayTrainingScheduleCardiorespiratoryFitnessActualCompletion<DayTrainingScheduleCardiorespiratoryFitnessActualCompletionVM>(studentId, body);

                    ConvertDayTrainingScheduleCardiorespiratoryFitnessActualCompletionVM convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion = new ConvertDayTrainingScheduleCardiorespiratoryFitnessActualCompletionVM();
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.exerciseIntensity = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.exerciseIntensity;
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.exerciseFrequency = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.exerciseFrequency;
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.heartRate = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.heartRate;
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.trainingUnitMinute = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.trainingUnitMinute;
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.sports = JsonConvert.DeserializeObject<List<string>>(dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.sports);
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.updateRecordId = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.updateRecordId;
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.updateRecordDate = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.updateRecordDate;
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.updateRecordTime = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.updateRecordTime;
                    convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion.actualCompletion = dayTrainingScheduleCardiorespiratoryFitnessActualCompletion.actualCompletion;

                    result.isSuccess = true;
                    result.data = convertDayTrainingScheduleCardiorespiratoryFitnessActualCompletion;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-肌力與肌耐力實際完成次數
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/day/trainingSchedule/{studentId:int}/muscleStrengthAndMuscleEndurance/actualCompletion")]
        public IActionResult GetDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion(int studentId, RequestTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得當日訓練排程-肌力與肌耐力實際完成次數
                    var dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion = new TrainingScheduleModels(conn).GetDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion<DayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionVM>(studentId, body);

                    ConvertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionVM convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion = new ConvertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionVM();

                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.exerciseIntensity = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.exerciseIntensity;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.heartRate = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.heartRate;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.exerciseFrequency = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.exerciseFrequency;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.trainingUnitGroup = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.trainingUnitGroup;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.trainingUnitNumber = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.trainingUnitNumber;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.updateRecordId = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.updateRecordId;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.updateRecordDate = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.updateRecordDate;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.updateRecordTime = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.updateRecordTime;
                    convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.finish = dayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion.finish;

                    result.isSuccess = true;
                    result.data = convertDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletion;
                    logger.LogInformation("GetDayTrainingScheduleMuscleStrengthAndMuscleEnduranceActualCompletionSuccess");
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-柔軟度實際完成次數
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/day/trainingSchedule/{studentId:int}/softness/actualCompletion")]
        public IActionResult GetDayTrainingScheduleSoftnessActualCompletion(int studentId, RequestTrainingScheduleSoftnessActualCompletionModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得當日訓練排程-柔軟度實際完成次數
                    var dayTrainingScheduleSoftnessActualCompletion = new TrainingScheduleModels(conn).GetDayTrainingScheduleSoftnessActualCompletion<DayTrainingScheduleSoftnessActualCompletionVM>(studentId, body);

                    ConvertDayTrainingScheduleSoftnessActualCompletionVM convertDayTrainingScheduleSoftnessActualCompletion = new ConvertDayTrainingScheduleSoftnessActualCompletionVM();

                    convertDayTrainingScheduleSoftnessActualCompletion.exerciseIntensity = dayTrainingScheduleSoftnessActualCompletion.exerciseIntensity;
                    convertDayTrainingScheduleSoftnessActualCompletion.heartRate = dayTrainingScheduleSoftnessActualCompletion.heartRate;
                    convertDayTrainingScheduleSoftnessActualCompletion.exerciseFrequency = dayTrainingScheduleSoftnessActualCompletion.exerciseFrequency;
                    convertDayTrainingScheduleSoftnessActualCompletion.trainingUnitMinute = dayTrainingScheduleSoftnessActualCompletion.trainingUnitMinute;
                    convertDayTrainingScheduleSoftnessActualCompletion.sports = JsonConvert.DeserializeObject<List<string>>(dayTrainingScheduleSoftnessActualCompletion.sports);
                    convertDayTrainingScheduleSoftnessActualCompletion.updateRecordId = dayTrainingScheduleSoftnessActualCompletion.updateRecordId;
                    convertDayTrainingScheduleSoftnessActualCompletion.updateRecordDate = dayTrainingScheduleSoftnessActualCompletion.updateRecordDate;
                    convertDayTrainingScheduleSoftnessActualCompletion.updateRecordTime = dayTrainingScheduleSoftnessActualCompletion.updateRecordTime;
                    convertDayTrainingScheduleSoftnessActualCompletion.actualCompletion = dayTrainingScheduleSoftnessActualCompletion.actualCompletion;

                    result.isSuccess = true;
                    result.data = convertDayTrainingScheduleSoftnessActualCompletion;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得訓練排程-平衡實際完成次數
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/day/trainingSchedule/{studentId:int}/balance/actualCompletion")]
        public IActionResult GetDayTrainingScheduleBalanceActualCompletion(int studentId, RequestTrainingScheduleBalanceActualCompletionModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();

                using(conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得當日訓練排程-柔軟度實際完成次數
                    var dayTrainingScheduleBalanceActualCompletion = new TrainingScheduleModels(conn).GetDayTrainingScheduleBalanceActualCompletion<DayTrainingScheduleBalanceActualCompletionVM>(studentId, body);

                    ConvertDayTrainingScheduleBalanceActualCompletionVM convertDayTrainingScheduleBalanceActualCompletion = new ConvertDayTrainingScheduleBalanceActualCompletionVM();

                    convertDayTrainingScheduleBalanceActualCompletion.exerciseIntensity = dayTrainingScheduleBalanceActualCompletion.exerciseIntensity;
                    convertDayTrainingScheduleBalanceActualCompletion.heartRate = dayTrainingScheduleBalanceActualCompletion.heartRate;
                    convertDayTrainingScheduleBalanceActualCompletion.exerciseFrequency = dayTrainingScheduleBalanceActualCompletion.exerciseFrequency;
                    convertDayTrainingScheduleBalanceActualCompletion.trainingUnitMinute = dayTrainingScheduleBalanceActualCompletion.trainingUnitMinute;
                    convertDayTrainingScheduleBalanceActualCompletion.sports = JsonConvert.DeserializeObject<List<string>>(dayTrainingScheduleBalanceActualCompletion.sports);
                    convertDayTrainingScheduleBalanceActualCompletion.updateRecordId = dayTrainingScheduleBalanceActualCompletion.updateRecordId;
                    convertDayTrainingScheduleBalanceActualCompletion.updateRecordDate = dayTrainingScheduleBalanceActualCompletion.updateRecordDate;
                    convertDayTrainingScheduleBalanceActualCompletion.updateRecordTime = dayTrainingScheduleBalanceActualCompletion.updateRecordTime;
                    convertDayTrainingScheduleBalanceActualCompletion.actualCompletion = dayTrainingScheduleBalanceActualCompletion.actualCompletion;

                    result.isSuccess = true;
                    result.data = convertDayTrainingScheduleBalanceActualCompletion;
                    return StatusCode((int)HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 更新訓練排程實際完成次數
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPatch]
        [Route("api/trainingSchedule/{studentId:int}/actualCompletion/replace")]
        public IActionResult UpdateTrainingScheduleActualCompletion(int studentId, RequestUpdateTrainingScheduleModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 檢查排程資料是否存在
                    var trainingScheduleExist = new TrainingScheduleModels(conn).InspactTrainingSchedule<InspactTrainingScheduleDataVM>(studentId, body.trainingType, body.scheduleDate);

                    // 如果排程資料不存在
                    if (trainingScheduleExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "TrainingScheduleDataExist";
                        logger.LogError("TrainingScheduleDataExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 取得過去 60 次訓練排程資料
                    var trainingScheduleReport = new TrainingScheduleModels(conn).GetBeforeTrainingScheduleReport<GetBeforeTrainingScheduleReportVM>(studentId, body.trainingType);

                    // 是否為過去 60 次訓練排程資料
                    bool isBeforeSixty = false;

                    // 檢查所選當日是否在過去 60 次內訓練排程資料
                    for (int i = 0; i < trainingScheduleReport.Count(); i++)
                    {
                        if (trainingScheduleReport.ToList()[i].scheduleDate == body.scheduleDate.ToString("yyyy-MM-dd"))
                        {
                            isBeforeSixty = true;
                            break;
                        }
                    }
                    
                    // 如果不為過去 60 次訓練排程資料
                    if (!isBeforeSixty)
                    {
                        result.isSuccess = false;
                        result.message = "NotModifyBeforeSixtyTrainingSchedule";
                        logger.LogError("NotModifyBeforeSixtyTrainingSchedule");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 更新訓練排程心肺適能實際完成次數
                    var updateTrainingScheduleCardiorespiratoryFitnessRow = new TrainingScheduleModels(conn).UpdateTrainingScheduleActualCompletion(studentId, recordId, body);

                    if (updateTrainingScheduleCardiorespiratoryFitnessRow == 1)
                    {
                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "UpdateTrainingScheduleCardiorespiratoryFitnessSucces";
                        logger.LogInformation("UpdateTrainingScheduleCardiorespiratoryFitnessSucces");
                        return StatusCode((int)HttpStatusCode.OK, result);
                    }
                    else
                    {
                        transaction.Rollback();
                        result.isSuccess = false;
                        result.message = "UpdateTrainingScheduleCardiorespiratoryFitnessFail";
                        logger.LogError("UpdateTrainingScheduleCardiorespiratoryFitnessFail");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增訓練排程日期
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/{studentId:int}/trainingSchedule/add")]
        public IActionResult AddTrainingSchedule(int studentId, RequestInsertTrainingScheduleWeekModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            // 取得這週剩餘天數範圍
            List<string> correctThisWeekDateRemainingDayRange = new DayUtils().GetThisWeekRemainingDayRange();

            bool isNotNextWeekRange = false; // 是否排程日期不再下星期範圍

            // 檢查下週是否有不存在這週剩餘範圍
            for (int i = 0; i < body.scheduleDate.Count(); i++)
            {
                int index = correctThisWeekDateRemainingDayRange.IndexOf(body.scheduleDate[i].ToString("yyyy-MM-dd"));
                if (index == -1)
                {
                    isNotNextWeekRange = true;
                    break;
                }
            }

            // 如果選擇日期不存在這週範圍
            if (isNotNextWeekRange)
            {
                result.isSuccess = false;
                result.message = "SelectDateNotThisWeekRange";
                logger.LogError("SelectDateNotThisWeekRange");
                return StatusCode((int)HttpStatusCode.BadRequest, result);
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int recordId = Convert.ToInt32(userInfo["id"]);

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 檢查學員是否存在
                    var student = new StudentModels(conn).InspactStudentExist<InspactStudentVM>(studentId);

                    // 如果找不到學員
                    if (student == null)
                    {
                        result.isSuccess = false;
                        result.message = "StudentDoesNotExist";
                        logger.LogError("StudentDoesNotExist");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 檢查訓練排程天數是否超出運動處方天數
                    var trainingScheduleInfo = new TrainingScheduleModels(conn).InspaceTrainingScheduleAndExercisePrescriptionDayContains<InspaceTrainingScheduleAndExercisePrescriptionDayContainsVM>(studentId, body);

                    // 如果訓練排程天數超出運動處方天數
                    if (!(body.scheduleDate.Count() <= trainingScheduleInfo.exerciseFrequency))
                    {
                        result.isSuccess = false;
                        result.message = "TrainingScheduleDayCountLargerThanExercisePrescriptionDayCount";
                        logger.LogError("TrainingScheduleDayCountLargerThanExercisePrescriptionDayCount");
                        return StatusCode((int)HttpStatusCode.Forbidden, result);
                    }

                    // 刪除舊的排程週
                    var deleteTrainingScheduleRow = new TrainingScheduleModels(conn).DeleteTrainingScheduleWeek(studentId, correctThisWeekDateRemainingDayRange, body);
                    
                    if (body.scheduleDate.Count() > 0)
                    {
                        // 新增排程週
                        var insertTrainingScheduleRow = new TrainingScheduleModels(conn).InsertTrainingScheduleWeek(studentId, recordId, body);

                        if (deleteTrainingScheduleRow >= 0 && insertTrainingScheduleRow > 0)
                        {
                            transaction.Commit();
                            result.isSuccess = true;
                            result.message = "InsertTrainingScheduleSuccess";
                            logger.LogInformation("InsertTrainingScheduleWeekSuccess");
                            return StatusCode((int)HttpStatusCode.Created, result);
                        }
                        else
                        {
                            transaction.Rollback();
                            result.isSuccess = false;
                            result.message = "InsertTrainingScheduleFail";
                            logger.LogError("InsertTrainingScheduleFail");
                            return StatusCode((int)HttpStatusCode.Forbidden, result);
                        }
                    }
                    else
                    {
                        transaction.Commit();
                        result.isSuccess = true;
                        result.message = "InsertTrainingScheduleSuccess";
                        logger.LogInformation("InsertTrainingScheduleSuccess");
                        return StatusCode((int)HttpStatusCode.Created, result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
