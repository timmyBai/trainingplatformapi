using Microsoft.Extensions.Logging;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace TrainingPlatformApi.Utils
{
    public class NetworkUtils
    {
        private int Port { get; set; } = 80;

        public string Domain { get; set; } = "";

        public string ImagePath { get; set; } = "";

        public NetworkUtils()
        {
            var builder = WebApplication.CreateBuilder();
            var app = builder.Build();
            var host = GetLocalAddress();

            if (app.Environment.IsDevelopment())
            {
                Port = 8080;
                Domain = $"http://{host}:{Port}";
                JwtUtils.Domain = Domain;
                ImagePath = "/DEV/TrainingPlatform/Image";
            }
            else if (app.Environment.IsStaging())
            {
                Port = 8081;
                Domain = $"http://{host}:{Port}";
                JwtUtils.Domain = Domain;
                ImagePath = "/Stag/TrainingPlatform/Image";
            }
            else if (app.Environment.IsProduction())
            {
                Domain = $"http://{host}";
                JwtUtils.Domain = Domain;
                ImagePath = "/TrainingPlatform/Image";
            }
            else
            {
                Domain = $"http://{host}";
                JwtUtils.Domain = Domain;
                ImagePath = "/TrainingPlatform/Image";
            }
        }

        private string GetLocalAddress()
        {
            string networkInterfaceName = "ens";
            string result = "";

            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var networkInterface in networkInterfaces)
            {
                if (networkInterface.Name.StartsWith(networkInterfaceName, StringComparison.OrdinalIgnoreCase))
                {
                    IPInterfaceProperties ipProperties = networkInterface.GetIPProperties();
                    foreach (var ipAddress in ipProperties.UnicastAddresses)
                    {
                        if (ipAddress.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            result = ipAddress.Address.ToString();
                            Console.WriteLine($"IP Address ({networkInterface.Name}): {ipAddress.Address}");
                        }
                    }
                }
            }

            return result;
        }
    }
}
