using System.Drawing;
using System.Drawing.Imaging;

namespace TrainingPlatformApi.Utils
{
    public class ImageUtils
    {
        /// <summary>
        /// 圖片大小 2 MB
        /// </summary>
        private int ImageSize = 2;

        /// <summary>
        /// 支援圖片副檔
        /// </summary>
        private string[] FileExtension = new string[] {
            "data:image/png;base64,",
            "data:image/gif;base64,",
            "data:image/jpg;base64,",
            "data:image/jpeg;base64,"
        };

        /// <summary>
        /// 檢查是否為合法副檔名
        /// </summary>
        /// <param name="base64Image">base64 圖片</param>
        /// <returns></returns>
        public bool InspactPictureFileExtension(string base64Image)
        {
            for (int i = 0; i < FileExtension.Length; i++)
            {
                if (base64Image.IndexOf(FileExtension[i]) != -1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 檢查副檔名為哪一種
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public int GetPictureFileExtensionIndex(string base64String)
        {
            for (int i = 0; i < FileExtension.Length; i++)
            {
                if (base64String.IndexOf(FileExtension[i]) != -1)
                {
                    return i + 1;
                }
            }

            return 0;
        }

        /// <summary>
        /// 檢查圖片容量大小
        /// </summary>
        /// <param name="base64Image">base64Image</param>
        /// <returns></returns>
        public bool InspactPictureSize(string base64Image)
        {
            string tempBase64Image = base64Image;
            FileExtension.ToList().ForEach((item) =>
            {
                tempBase64Image = tempBase64Image.Replace(item, "");
            });

            decimal stringLength = tempBase64Image.Length;
            decimal base64FileByte = stringLength - Math.Ceiling((decimal)stringLength / 8) * 2;
            decimal base64FileKB = base64FileByte / 1024;
            decimal base64FileMB = base64FileKB / 1024;

            if (base64FileMB > ImageSize)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 圖片抓取轉換 base64 字串
        /// </summary>
        /// <param name="imageFilePath">圖片路徑</param>
        /// <returns></returns>
        public List<string> InspactImageFilePathDoesNotExist(string userId)
        {
            string imageFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}ServerImage/{userId}";

            if (Directory.Exists(imageFilePath))
            {
                List<string> fileList = new List<string>();
                foreach(string name in Directory.GetFiles(imageFilePath))
                {
                    fileList.Add(name);
                }
                
                if (fileList.Count > 0)
                {
                    return fileList;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// base64 保存
        /// </summary>
        /// <param name="base64String">base64String 字串</param>
        /// <returns></returns>
        public Bitmap Base64StringConvertImage(string base64String, string savePath, string imageName = "")
        {
            // 取得附檔名
            int fileExtensionIndex = GetPictureFileExtensionIndex(base64String);

            // 檢查是否有附檔名
            if (fileExtensionIndex == 0)
            {
                return null;
            }

            string base64StringByte = base64String.Replace(FileExtension[fileExtensionIndex - 1], "");
            string fileExtension = FileExtension[fileExtensionIndex - 1].Replace("data:image/", "").Replace(";base64,", "");

            byte[] arr = Convert.FromBase64String(base64StringByte);
            MemoryStream ms = new MemoryStream(arr);
            Bitmap bmp = new Bitmap(ms);

            // 檢查目標圖片 id 是否存在
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }

            // 儲存目標圖片位置
            switch (fileExtensionIndex)
            {
                case 1:
                    bmp.Save($@"{savePath}/{imageName}.{fileExtension}", ImageFormat.Png);
                    break;
                case 2:
                    bmp.Save($@"{savePath}/{imageName}.{fileExtension}", ImageFormat.Gif);
                    break;
                case 3:
                    bmp.Save($@"{savePath}/{imageName}.{fileExtension}", ImageFormat.Jpeg);
                    break;
                default:
                    bmp.Save($@"{savePath}/{imageName}.{fileExtension}", ImageFormat.Jpeg);
                    break;
            }
            
            ms.Close();

            return bmp;
        }
    }
}
